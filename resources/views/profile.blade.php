<!DOCTYPE html>
<html>
<head>
	<title>profile</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="google-signin-client_id" content="765849838126-5ufs3eppqcmbjk26n7ljph8kdsv844uh.apps.googleusercontent.com">

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<a href="#" onclick="signOut();">Sign out</a>

  <script src="{{ asset('js/app.js') }}"></script>
	<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>

<script type="text/javascript">
	function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
          document.cookie = "G_AUTHUSER_H=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          console.log('User signed out.');

          let checkCookie = document.cookie
	        if (checkCookie.match('G_AUTHUSER_H') == null) {
	            setTimeout(function (){
		            window.location.assign('http://localhost:8000/login')
		        }, 500);
	        }
        });

        // let checkCookie = document.cookie
        // let btnSignin = document.getElementById('g-signin-btn')
        // let btnSignout = document.getElementById('g-signout-btn')
        // if (checkCookie.match('G_AUTHUSER_H') == null) {
        //     btnSignin.classList.remove('hide')
        //     btnSignout.classList.add('hide')
        // } else {
        //     btnSignin.classList.add('hide')
        //     btnSignout.classList.remove('hide')
        // }
    }

    function onLoad() {
      gapi.load('auth2', function() {
        gapi.auth2.init();
      });
    }
</script>
</body>
</html>