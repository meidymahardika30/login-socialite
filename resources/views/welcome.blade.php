<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-signin-client_id" content="765849838126-5ufs3eppqcmbjk26n7ljph8kdsv844uh.apps.googleusercontent.com">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"><!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <div id="app">
    
        </div>

        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 

        <script type="text/javascript">
            function onSignIn(googleUser) {
                console.log("on sign in, granted scopes: " + googleUser.getGrantedScopes());
                console.log("ID token: " + googleUser.getAuthResponse().id_token);

                var profile = googleUser.getBasicProfile();
                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

                // setTimeout(function (){
                //     window.location.assign('/Home')
                // }, 500);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-tOKEN': $('meta[name="csrf-token"]')
                    }
                });

                $.ajax({
                    type: 'GET',
                    url: '{{ url('/store') }}',
                    data: {
                        "name" : profile.getName(),
                        "email" : profile.getEmail(),
                        "access_token" : googleUser.getAuthResponse().id_token,
                    },
                    dataType: 'json',
                    success: function(response){
                        console.log('success');
                    }
                });
            }

            function signOut() {
                var auth2 = gapi.auth2.getAuthInstance();
                auth2.signOut().then(function () {
                  document.cookie = "G_AUTHUSER_H=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                  console.log('User signed out.');

                  // let checkCookie = document.cookie
                  //   if (checkCookie.match('G_AUTHUSER_H') == null) {
                  //       setTimeout(function (){
                  //           window.location.assign('http://localhost:8000/login')
                  //       }, 500);
                  //   }
                });
            }

            function onLoad() {
              gapi.load('auth2', function() {
                gapi.auth2.init();
              });
            }
        </script>
    </body>
</html>
