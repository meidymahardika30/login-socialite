@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="fb-login-button" data-width="" data-size="medium" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false" onclick="loginFacebook()"></div> --}}

                        {{-- <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <a href="javascript:void(0)" value="popup" onClick="loginFacebook()">Login with Facebook</a>
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <a href="javascript:void(0)" value="popup" onClick="loginGoogle()">Login with Google</a>
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <a href="{{ url('/auth/google') }}">Login with Google</a>
                            </div>
                        </div> --}}

                        <br>
                        <div align="center" id="g-signin" class="g-signin2" data-onsuccess="onSignIn"></div>
                        {{-- <a href="#" onclick="signOut();">Sign out</a> --}}

                        {{-- <body onload="init()">
                            <div id="g-signin-btn" class="g-signin-btn2 hide"></div>
                            <div><a href="#" onclick="disconnect();">Disconnect</a></div>
                            <div><img id="profileImage" src="" /></div>
                            <textarea id="message" cols="80" rows="10"></textarea>
                        </body> --}}

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://apis.google.com/js/platform.js" async defer></script>

{{-- <script type="text/javascript">
    function loginGoogle() {
        var win = window.open('{{ url('/auth/google') }}','popup', 'height=600,width=600, top=100, left=400');
    }

    function loginFacebook() {
        var win = window.open('{{ url('/auth/facebook') }}','popup', 'height=600,width=600, top=100, left=400');
    }
</script> --}}

<script type="text/javascript">
    // function init() {
    //     gapi.load('auth2', function() {
    //         auth2 = gapi.auth2.init({
    //             client_id: '765849838126-5ufs3eppqcmbjk26n7ljph8kdsv844uh.apps.googleusercontent.com',
    //             fetch_basic_profile: false,
    //             scope: 'profile',
    //             redirect_uri: 'htttp://localhost:8000/auth/google/callback'
    //         });
    //         gapi.signin2.render("g-signin-btn", {
    //             scope: 'email',
    //             width: 200,
    //             height: 50,
    //             longtitle: false,
    //             theme: 'dark',
    //             onsuccess: onSignIn,
    //             onfailure: null
    //         });
    //     });
    // }

    function onSignIn(googleUser) {
        console.log("on sign in, granted scopes: " + googleUser.getGrantedScopes());
        console.log("ID token: " + googleUser.getAuthResponse().id_token);

        var profile = googleUser.getBasicProfile();
        console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

        setTimeout(function (){
            window.location.assign('http://localhost:8000/profile')
        }, 500);
    }

    // function signOut() {
    //     var auth2 = gapi.auth2.getAuthInstance();
    //     auth2.signOut().then(function () {
    //       document.cookie = "G_AUTHUSER_H=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    //       console.log('User signed out.');
    //     });

    //     setTimeout(function (){
    //         window.location.assign('http://localhost:8000/login')
    //     }, 1000);

    //     // let checkCookie = document.cookie
    //     // let btnSignin = document.getElementById('g-signin-btn')
    //     // let btnSignout = document.getElementById('g-signout-btn')
    //     // if (checkCookie.match('G_AUTHUSER_H') == null) {
    //     //     btnSignin.classList.remove('hide')
    //     //     btnSignout.classList.add('hide')
    //     // } else {
    //     //     btnSignin.classList.add('hide')
    //     //     btnSignout.classList.remove('hide')
    //     // }
    // }
</script>

{{-- <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v4.0&appId=393127484719966&autoLogAppEvents=1"></script>

<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>



<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '393127484719966',
      cookie     : true,
      xfbml      : true,
      version    : 'v4.0'
    });
      
     
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    function loginFacebook(){
        FB.login(function(response) {
            window.location.replace("{{ url('/auth/facebook/callback') }}");
        });
    }
</script> --}}

 @endsection
