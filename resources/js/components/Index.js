import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Home from './Home';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class Index extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Home</div>

                            <div className="card-body">I'm an Home component!</div>
                            <div align="center" id="g-signin" className="g-signin2" data-onsuccess="onSignIn"></div>
                            <a href="#" onClick={ () => signOut() }>Sign out</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// componentDidMount(){
//     const script = document.createElement("script");

//     function onSignIn(googleUser) {
//         console.log("on sign in, granted scopes: " + googleUser.getGrantedScopes());
//         console.log("ID token: " + googleUser.getAuthResponse().id_token);

//         var profile = googleUser.getBasicProfile();
//         console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
//         console.log('Name: ' + profile.getName());
//         console.log('Image URL: ' + profile.getImageUrl());
//         console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

//         // setTimeout(function (){
//         //     window.location.assign('/Home')
//         // }, 500);

//         ReactDOM.render(<Route path="/Home"></Route>)
//     }

//     function signOut() {
//         var auth2 = gapi.auth2.getAuthInstance();
//         auth2.signOut().then(function () {
//           document.cookie = "G_AUTHUSER_H=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
//           console.log('User signed out.');

//           // let checkCookie = document.cookie
//           //   if (checkCookie.match('G_AUTHUSER_H') == null) {
//           //       setTimeout(function (){
//           //           window.location.assign('http://localhost:8000/login')
//           //       }, 500);
//           //   }
//         });
//     }
// }


if (document.getElementById('app')) {
    ReactDOM.render(<Index />, document.getElementById('app'));
}
