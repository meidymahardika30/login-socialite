<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobseeker extends Model
{
    protected $fillable = [
        'name', 'email', 'password',
    ];

    // public function socialAccounts()
    // {
    //     return $this->hasMany(SocialAccount::class);
    // }
}
