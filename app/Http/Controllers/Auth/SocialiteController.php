<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\SocialAccount;
use App\User;
use Illuminate\Support\Facades\Auth;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider)
    {
    	return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
    	try {
    		$user = Socialite::driver($provider)->stateless()->user();
    	} catch (Exception $e) {
    		return redirect('/login');
    	}

    	$authUser = $this->findOrCreateUser($user, $provider);

    	Auth::login($authUser, true);

        User::where('id',$authUser->id)->update([
            'access_token' => $user->token
        ]);

        // return $authUser->id;

        // echo "<script>window.close(); location.reload();</script>";

        return redirect('/home');

        // return json_encode($user->token);
    }

    public function findOrCreateUser($socialUser, $provider)
    {
    	$SocialAccount = SocialAccount::where('provider_id', $socialUser->getId())
    					->where('provider', $provider)
    					->first();

    	if ($SocialAccount) {
    		return $SocialAccount->user;
    	}else{
    		$user = User::where('email', $socialUser->getEmail())->first();

    		if (!$user) {
    			$user = User::create([
    				'name' => $socialUser->getName(),
    				'email' => $socialUser->getEmail()
    			]);
    		}

    		$user->socialAccounts()->create([
    			'provider_id' => $socialUser->GetId(),
    			'provider' => $provider
    		]);

    		// return $user;

            return response()->json($user);
    	}
    }

    public function store(Request $request)
    {
        $email = User::where('email',$request->email)->select('email')->value('email');
        
        if ($email) {
            $input['access_token'] = $request->access_token;
            $user = User::where('email',$request->email)->update($input);
        } else {
            $input = $request->all();
            $user = User::create($input);
        }

        return response()->json($user);
    }

    // public function update(Request $request)
    // {
    //     $email = User::where('email',$request->email)->select('email')->value('email');

    //     if ($email) {
    //         $input['access_token'] = $request->access_token;
    //         $user = User::update($input);
    //     } else {
    //         $input = $request->all();
    //         $user = User::create($input);
    //     }

    //     return response()->json($user);
    // }
}
