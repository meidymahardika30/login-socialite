<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/store', 'Auth\SocialiteController@store');
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/beranda', function() {
// 	return view('beranda');
// });
// Route::get('auth/{provider}', 'Auth\SocialiteController@redirectToProvider');
// Route::get('auth/{provider}/callback', 'Auth\SocialiteController@handleProviderCallback');
// Route::get('auth/findOrCreateUser', 'Auth\SocialiteController@findOrCreateUser');
